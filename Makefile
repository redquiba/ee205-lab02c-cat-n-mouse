###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 02c - catNmouse - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
###
### @author Ryan Edquiba 
### @date   1/26/22 
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = catNmouse

all: $(TARGET)

catNmouse: catNmouse.c
	$(CC) $(CFLAGS) -o $(TARGET) catNmouse.c

clean:
	rm -f $(TARGET) *.o


///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Ryan Edquiba  <redquiba@hawaii.edu>
/// @date    1/27/22
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DEFAULT_MAX_NUMBER (2048)
#define RAND(n) (rand() % n) 

int main( int argc, char* argv[] ) 
{
   printf( "Cat `n Mouse\n" );
  // printf( "The number of arguments is: %d\n", argc );

   srand(time(NULL));
   int GUESS;
   int MAX_VALUE;

   if (argc == 1)
   {
      MAX_VALUE = DEFAULT_MAX_NUMBER;
     //  printf("Max Number is %d\n", MAX_VALUE);
   }

   else if (argc > 1)
   {
      MAX_VALUE = atoi( argv[1] );
     //  printf( "%d\n",MAX_VALUE );

   }

   else
   {
      printf( "No cat, That's not gonna work\n");
      exit(1);
   }

    int RANDOM_NUM = RAND(MAX_VALUE);
         if (RANDOM_NUM == 0)
         {
           // printf("RANDOM_NUM = 0\n");
           RANDOM_NUM = rand() % MAX_VALUE;
         }
    // printf( "%d\n", RANDOM_NUM);


      do
      {
         printf( "Ok cat, I'm thinking of a number from 1 to %d. Make a guess: ", MAX_VALUE);
         if (scanf( "%d", &GUESS) == 1)
         {
            if (GUESS < 1)
            {
               printf("Hey cat, guess a positive integer greater than 1\n");
               continue;
            }
            else if (GUESS > MAX_VALUE)
            {
               printf("Hey cat, guess a number less than %d\n", MAX_VALUE);
               continue;
            }
            else if (GUESS > RANDOM_NUM)
            {
               printf( "No cat... the number I'm thinking of is smaller than %d\n", GUESS);
               continue;
            }
            else if (GUESS < RANDOM_NUM)
            {
               printf( "No cat... the number I'm thinking of is bigger than %d\n", GUESS);
              continue;
            }
            else
            {
               break;
            }
         }
         else
         {
            printf( "Hey cat, That's not a number. I don't wanna play with you anymore.\n");
            return 1;
         }
      }while( GUESS != RANDOM_NUM );
   

   printf("You got me\n");

printf("                 _                                 \n");
printf("                 \\`*-.                            \n");
printf("                  )  _`-.                          \n");
printf("                  .  : `. .                        \n");
printf("                  : _   \'  \\                     \n"); 
printf("                  ; *` _.   `*-._                  \n");
printf("                  `-.-'          `-.               \n");
printf("                     ;       `       `.            \n");
printf("                      :.       .        \\         \n");
printf("                      . \\  .   :   .-\'   .       \n");
printf("                      \'  `+.;  ;  \'      :       \n");
printf("                      :  \'  |    ;       ;-.      \n");
printf("    .b--.            ; \'   : :`-:     _.`* ;      \n");
printf("    `=,-,-\'~~~      .*\' /  .*\' ; .*`- +\'  `*\' \n");
printf("                   `*-*   `*-*  `*-*\'             \n");

   return 0;
}

